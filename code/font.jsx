import React from "react";
import { render } from "react-dom";

// get our fontawesome imports
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// create our App
const App = () => (
  <FontAwesomeIcon icon={faHome} />
);

export default App;
// render to #root
// render(<App />, document.getElementById("root"));