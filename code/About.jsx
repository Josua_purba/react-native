// import react from "react";
import React, { Component } from "react";
import { StyleSheet, View, Text, Image, ImageBackground, ScrollView } from "react-native";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Daftar(){
    return(
            <ScrollView style ={styles.ScrollView}>
        <View style = {styles.container}>
            <View style = {styles.header}>
                <Text style = {styles.title}>About Us</Text>
                <Image style = {styles.gambar} source={require('./image/jesus.jpeg')} />
                <View style = {styles.status}>
                <Text style = {styles.intro}>{` I am,\nJosua Purba`}</Text>
                <Text>React Native Developer</Text>
                </View>
            </View>
            <View style = {styles.body}>
                <Text style = {styles.conten}>{`I'm a React Native Developer. And this is the first application that I made using react native technology. I hope with this application I can help your problem. if you have problems with this application you can contact me via or if you need me to solve your problems`}</Text>
            </View>
            <View style = {styles.contact}>
                <Text style = {styles.title}>My Contact</Text>
            </View>
        </View>
        <FontAwesomeIcon
                    style={styles.iconStyle}
                    icon={faHome}
                    />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
		alignItems: "center",
		justifyContent: "center",
        backgroundColor: '#00BFFF',
        paddingHorizontal: 5,
        paddingTop: 50,
        borderWidth: 5,
        height: '100%'
    }, 
    ScrollView : {
        // backgroundColor : 'black',
        marginHorizontal: 0
    },
    header: {
        borderWidth: 2,
        padding: 10,
        justifyContent: "flex-end",
        bottom: '20%',
        fontFamily: "Cochin",
        width: '90%',
        backgroundColor: 'gray',
        borderRadius: 10
    },
    title : {
        fontFamily: "sans-serif-thin",
        fontSize: 30,
        textAlign: 'center',
        marginBottom: 20,
        color: 'black',
        fontWeight: 'bold'
    },
    intro : {
        fontSize: 20,
        // marginVertical: 5,
        fontFamily: "sans-serif-thin",
        fontWeight: 'bold',
    },
    status : {
        // paddingHorizontal: 50,
        // borderWidth: 5,
        position: 'relative',
        width: '60%',
        bottom: '20%'
    },
    gambar : {
        width : 100,
        height : 100,
        borderWidth: 10,
        alignSelf: 'flex-end',
        top: '15%',
        borderRadius: 50
    },
    body : {
        borderWidth: 3,
        bottom: '15%',
        padding: 10,
        width: '90%',
        backgroundColor: 'grey',
        borderRadius: 10
    },
    conten : {
        fontSize: 18,
        margin: 5,
        letterSpacing: 1,
        fontStyle: 'italic'
    }
})