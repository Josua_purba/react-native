import React from "react";
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity, ImageBackground } from "react-native";
import { color } from "react-native/Libraries/Components/View/ReactNativeStyleAttributes";

export default function LoginScreen(){
    return(
        <View style = {styles.container}>
            {/* <ImageBackground style={styles.image} source={require('./gambar.jpg')} resizeMode="cover"> */}
            <View style = {styles.Component}>
            <Text style = {{fontSize: 40, fontFamily: 'monospace', bottom: '5%', color: 'black', margin: 10}}>LOGIN</Text>
            <View style = {styles.input}>
            <Text style={{margin: 10, marginBottom: 20, fontSize: 15, fontWeight: 'bold', color: 'black', fontStyle: 'italic'}}>USERNAME</Text>
            <TextInput style = {{color: 'black', borderWidth: 2, width: 100, padding: 10,bottom : '10%', borderRadius: 10, minWidth: 250, paddingLeft: 20, margin: 5}}placeholder = {'UserName'}></TextInput>
            </View>

            <View style = {styles.input}>
            <Text style={{margin: 10, fontSize: 15, fontWeight: 'bold', color: 'black', fontStyle: 'italic'}}>PASSWORD</Text>
            <TextInput secureTextEntry style = {{color: 'black', borderWidth: 2, width: 100, padding: 10,bottom : '5%', borderRadius: 10, minWidth: 250, paddingLeft: 20, margin: 5}}placeholder = {'Password'}></TextInput>
            </View> 

            <TouchableOpacity style={styles.tombol}>
                <TouchableOpacity style={styles.masuk}><Text style={{fontSize: 15, color:'white'}}>Masuk</Text></TouchableOpacity>
                <TouchableOpacity style={styles.Daftar}><Text style={{fontSize: 15, color:'white'}}>Daftar</Text></TouchableOpacity>
            </TouchableOpacity>

            </View>

           {/* <FormTextInput title={"Masuk Josua"}/> */}
           {/* </ImageBackground> */}
        </View>
        
    );
}



const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        // backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center",
        backgroundColor: '#00BFFF',
        // backgroundColor : '#dcdcdc'
        // borderWidth: 9
        paddingHorizontal: 10,
    },
    Component : {
        // borderWidth: 2,
        // height: '80%',
        // width: '100%'
        alignItems: 'center',
        justifyContent: 'center',
        bottom: 20

    },
    input : {
        bottom: 70,
        // borderWidth: 2,
        top: 30
    },
    button : {
        backgroundColor: 'white',
        top: 80,
        // borderWidth: 3,
        display: 'flex'
    },
    Daftar :{
        // backgroundColor: 'brown',
        margin: 10,
        // backgroundColor: 'blue',
        // borderWidth: 2,
        borderRadius: 10,
        backgroundColor: '#0000ff',
        padding: 10,
        paddingHorizontal: 40
    },
    tombol : {
        alignItems: "center",
        // backgroundColor: "#DDDDDD",
        padding: 10,
        // width: 200,
        borderRadius: 5,
        top: 60,
        // margin: 50,
        // display: 'none',
        flexDirection: 'row',
        justifyContent: 'center',
        // borderWidth: 3

    },
    masuk : {
        fontFamily: 'Arial',
        fontSize: 100,
        padding: 10,
        margin: 10,
        paddingHorizontal: 40, 
        // borderWidth: 2,
        borderRadius: 10,
        backgroundColor: '#0000e6',
        
    }
});