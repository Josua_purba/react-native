import React from 'react';
import ReactDOM from 'react-dom';
import { Text,StyleSheet,View } from 'react-native';

export default class learnState extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      type : 'Mustag',
      color : 'black',
      years : '2000',
      brand : 'Tesla',
      Name : 'Yamaha'
    }
  }
  render(){
    return(
          <View style = {style.container}>
      <Text>
          this {this.state.color}
          with Jenis : {this.state.type}
          Berwarna : {this.state.color}
          keluaran Tahun : {this.state.years}
          With Brand From : {this.state.brand}
        </Text>
        </View>
    )
  }
}
const style = StyleSheet.create({
  container : {
    flex: 1,
		alignItems: "center",
		justifyContent: "center",

  }
})
// ReactDOM.render(<learnState />, document.getElementById('root'))