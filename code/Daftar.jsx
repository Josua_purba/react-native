import React from "react";
import { StyleSheet, TextInput, View, Button, Text, TouchableOpacity, ScrollView } from "react-native";
import { backgroundColor } from "react-native/Libraries/Components/View/ReactNativeStyleAttributes";

export default function Daftar(){
    return(
        
        <View style = {styles.container}>
            
            <Text style = {styles.title}>Daftar</Text>
            <View style = {styles.form}>
                <View style = {styles.userName}>
                    <Text style = {styles.judulInput}>Nama Pengguna (UserName)</Text>
                    <TextInput style = {styles.TextInput} placeholder = {'UserName'}></TextInput>
                </View>
                <View style = {styles.Email}>
                    <Text style = {styles.judulInput}>Email</Text>
                    <TextInput style = {styles.TextInput} placeholder = {'Email'}></TextInput>
                </View>
                <View style = {styles.Password}>
                    <Text style = {styles.judulInput}>Masukkan Password</Text>
                    <TextInput style = {styles.TextInput} secureTextEntry = {true} placeholder = {'Masukkan Password'}></TextInput>
                </View>
                <View style = {styles.ulangiPassword}>
                    <Text style = {styles.judulInput}>Ulangi Password</Text>
                    <TextInput style = {styles.TextInput} secureTextEntry = {true} placeholder = {'Ulangi Password'}></TextInput>
                </View>

            </View>
            <View style = {styles.button}>
                <TouchableOpacity style = {styles.submit}>
                        <Text style = {styles.textbutton}>Submit</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.Cancel}>
                        <Text style = {styles.textbutton}>Cancel</Text>
                </TouchableOpacity>
            </View>
            {/* <TouchableOpacity style={styles.tombol}>
                <TouchableOpacity style={styles.masuk}><Text style={{fontSize: 15, color:'white'}}>Masuk</Text></TouchableOpacity>
                <TouchableOpacity style={styles.Daftar}><Text style={{fontSize: 15, color:'white'}}>Daftar</Text></TouchableOpacity>
            </TouchableOpacity> */}
            
           {/* <FormTextInput title={"Masuk Josua"}/> */}
           {/* </ImageBackground> */}
           
        </View>
        
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
		alignItems: "center",
		justifyContent: "center",
        backgroundColor: '#00BFFF',
        // paddingHorizontal: 5
    }, 
   title: {
       fontSize: 40, 
       fontFamily: 'monospace', 
       bottom: 15,
       color: 'black', 
       margin: 0
    },
    form : {
        top: 30
    },
    TextInput: {
        borderWidth: 2,
        padding: 8,
        borderRadius: 10,
        minWidth: 250,
    },
    judulInput: {
        marginVertical: 10,
        fontSize: 15,
        color: 'black',
        fontWeight: 'bold'
    },
    button : {
        top: 50,
        padding: 20,
        // borderWidth: 2
    },
    submit : {
        paddingHorizontal: 40,
        // borderWidth: 3,
        padding: 10,
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#0000e6'
    },
    Cancel : {
        paddingHorizontal: 40,
        // borderWidth: 3,
        padding: 10,
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#0000e6',
    },
    textbutton: {
        color: 'white',
        fontSize: 15
    },
    scrollView : {
        paddingHorizontal: 20,
        backgroundColor: 'black'
    }
})